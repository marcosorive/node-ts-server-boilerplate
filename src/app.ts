"use strict";
import * as express from "express";
import * as cors from 'cors';
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as keys from "../config/keys";
import { Bootstrap } from "./Bootstrap";

export class App {

    private readonly app: express.Application;
    private static readonly db: string = keys["mongoURI"];

    constructor(private readonly port: number) {
        this.port = port;
        this.app = express();
    }

    public startApp(): void {
        //This process.env.PORT is so the app works correctly in Heroku.
        //That reads the port from enviorment variables insetead of the one hardcorded here.
        this.app.listen(process.env.PORT || this.port, () => {
            new Bootstrap().setup();
            this.config();            
        });
        console.log("Server listening!");
    }

    private config(): void {
        this.app.use(cors());
        this.app.use(bodyParser.json({ limit: "1mb" }));
        // Support for application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false, limit: "1mb" }));
        // Setting public folder
        this.app.use(express.static(__dirname+'/public'));
        // Setting routes
        this.app.use('/api/product', Bootstrap.productRoutes);

        /* Serving frontend Uncoment this if you want to server a frontend.
        // this.app.use("*",(req: Request, res: Response) => {
        //     res.sendFile(path.join(__dirname+'/public/index.html'));
         }) */

        // Connecting DB
        mongoose.connect(App.db, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
            .then(() => console.log("MongoDB Connected!"))
            .catch((error) => console.log("Error connecting MongoDB: " + error));
    }
}