import { Request, Response } from "express";
import {IProduct} from '../interfaces/IProduct';
import { Bootstrap } from '../Bootstrap';

export class ProductController{

    public static async getAllProducts(req: Request, res: Response): Promise<void> {
        try{
            const products: IProduct[] = await Bootstrap.productService.getAllProducts();
            res.status(200).send(products);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductController.getAllProducts " + error);
            res.status(500).send();
        }
    }

    public static async getProduct(req: Request, res: Response): Promise<void>{
        try {
            const product: IProduct = await Bootstrap.productService.getProduct(req.params.id);
            if(product === undefined){
                res.status(404).send();
            }else{
                res.status(200).send(product);
            }            
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductController.getProduct " + error);
            res.status(500).send();
        }
    }

    public static async addProduct(req: Request, res: Response): Promise<void>{
        try{
            const product: IProduct = await Bootstrap.productService.addProduct(req.body);
            res.status(200).send(product);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductController.addProduct: " + error);
            res.status(500).send({error: error})
        }
    }

    public static async getProductsByCategory(req: Request, res: Response): Promise<void>{
        try{
            const product: IProduct[] = await Bootstrap.productService.getProductsByCategory(req.params.category);
            res.status(200).send(product);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductController.getProductsByCategory: " + error);
            res.status(500).send()
        }
    }

    public static async updateProduct(req: Request, res: Response): Promise<void>{
        try{
            const product: IProduct = await Bootstrap.productService.updateProduct(req.params.id, req.body);
            res.status(200).send(product);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductController.updateProduct: " + error);
            res.status(500).send()
        }
    }

    public static async deleteProduct(req: Request, res: Response): Promise<void>{
        try{
            if(await Bootstrap.productService.deleteProduct(req.params.id)){
                res.status(200).send();
            }
            else{
                res.status(404).send();
            }
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductController.deleteProduct: " + error);
            res.status(500).send({error: error})
        }
    }
}