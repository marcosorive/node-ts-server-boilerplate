"use strict";
import * as mongoose from 'mongoose';
import {Category} from '../enums/product/Category';
export interface IProduct extends mongoose.Document {
    name: String;
    slug: String; 
    description: String; 
    dateAdded: Date;
    stock: Number;
    category: Category;
};