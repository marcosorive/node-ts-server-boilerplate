import * as mongoose from 'mongoose';
import { IProduct } from '../interfaces/IProduct'

const productSchema = new mongoose.Schema({
    name: { type: String, required: true },
    slug: { type: String, required: true },
    description: { type: String, required: false },
    dateAdded: { type: Date, default: Date.now },
    stock: {type: Number},
    category: {type: Number},
});

export const Product = mongoose.model<IProduct>("Product", productSchema);