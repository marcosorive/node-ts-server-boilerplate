import {IProductRepository} from '../repositories/product/IProductRepository';
import { IProduct } from 'interfaces/IProduct';
import { Bootstrap } from '../Bootstrap';
import { Product } from '../models/Product';

export class ProductService{

    constructor(private productRepository: IProductRepository){}

    public getAllProducts(): Promise<IProduct[]>{
        try {
            return this.productRepository.getAll();
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductService.getAllProducts: " + error);
            throw new Error(error);
        }
    }

    public async getProduct(id: String): Promise<IProduct>{
        try{
            return await this.productRepository.getProduct(id);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductService.getProduct: " + error);
            throw new Error(error);
        }
    }

    public async addProduct(requestProduct: any): Promise<IProduct>{
        try {
            const product = this.getProductFromRequestProduct(requestProduct);
            return await this.productRepository.addProduct(product);
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductService.addProduct: " + error);
            throw new Error(error);
        }
    }

    public async getProductsByCategory(category: string): Promise<IProduct[]>{
        try{
            return await this.productRepository.getProductsByCategory(parseInt(category));
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductService.getProductsByCategoryt: " + error);
            throw new Error(error);
        }
    }

    public async updateProduct(id:string, requestProduct: any): Promise<IProduct>{
        try {
            const product = this.getProductFromRequestProduct(requestProduct);
            return await this.productRepository.updateProduct(id,product);
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductService.updateProduct: " + error);
            throw new Error(error);
        }
    }

    public async deleteProduct(id:string): Promise<boolean>{
        try {
            return await this.productRepository.deleteProduct(id);
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductService.deleteProduct: " + error);
            throw new Error(error);
        }
    }

    private getProductFromRequestProduct(requestProduct: any): IProduct{
        let product: IProduct = new Product();
        product.name = requestProduct.name;
        product.slug = this.stringToSlug(requestProduct.name);
        product.description = requestProduct.description;
        product.stock = parseInt(requestProduct.stock);
        product.category = parseInt(requestProduct.category);
        return product;
    }
    
    private stringToSlug (str: String): String {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();
      
        // remove accents, swap ñ for n, etc
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to   = "aaaaeeeeiiiioooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }
    
        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes
    
        return str;
    }
}