import { Logger } from 'winston';
import { Router } from 'express';
import { getProductRoutes } from './routes/ProductRoutes';
import { ProductService } from "./services/ProductService";
import { ProductRepository } from './repositories/product/ProductRepository';
import { Product } from './models/Product';
import { logger } from './utils/logger';

export class Bootstrap{
    public static productRoutes: Router;
    public static productService: ProductService;
    public static logger: Logger;

    constructor(){}

    public setup(): void{
        const productRepository = new ProductRepository(Product);
        const productService = new ProductService(productRepository);

        const productRoutes = getProductRoutes();

        Bootstrap.productRoutes = productRoutes;
        Bootstrap.productService = productService;
        Bootstrap.logger = logger;
    }
}