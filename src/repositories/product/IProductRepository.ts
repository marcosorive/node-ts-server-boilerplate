import {IProduct} from '../../interfaces/IProduct';

export interface IProductRepository {
    getAll(): Promise<IProduct[]>;
    getProduct(id: String): Promise<IProduct>;
    addProduct(product: IProduct): Promise<IProduct>;
    getProductsByCategory(category: Number): Promise<IProduct[]>;
    updateProduct(id: string, product: IProduct): Promise<IProduct>;
    deleteProduct(id: string): Promise<boolean>;
}