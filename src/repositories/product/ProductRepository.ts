"use strict";
import { IProductRepository } from './IProductRepository';
import { IProduct } from '../../interfaces/IProduct'
import { Model } from 'mongoose';
import { Bootstrap } from '../../Bootstrap';

export class ProductRepository implements IProductRepository{

    constructor(private Product: Model<IProduct>){}

    public async getAll(): Promise<IProduct[]>{
        try{
            return await this.Product.find();
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductRepository.getAll " + error);
            return undefined;
        }
    }

    public async getProduct(id: String): Promise<IProduct>{
        try{
            return await this.Product.findById(id);
        }
        catch(error){
            Bootstrap.logger.log("error","Error in ProductRepository.getProduct " + error);
            throw new Error();
        }
    }

    public async addProduct(product: IProduct): Promise<IProduct>{
        try {
            return await product.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductRepository.addProduct " + error);
            throw new Error();
        }        
    }

    public async getProductsByCategory(category: Number): Promise<IProduct[]>{
        try {
            return await this.Product.find({category : category} as IProduct);
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductRepository.getProductsByCategory " + error);
            throw new Error();
        }
    }

    public async updateProduct(id: string, newProduct: IProduct): Promise<IProduct>{
        try {
            const product = await this.Product.findById(id);
            product.name = newProduct.name;
            product.slug = newProduct.slug;
            product.description = newProduct.description;
            product.stock = newProduct.stock;
            product.category = newProduct.category;
            return await product.save();
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductRepository.updateProduct " + error);
            throw new Error();
        }
    }

    public async deleteProduct(id: string): Promise<boolean>{
        try {
            return (await this.Product.deleteOne({_id: id})).n === 1;
        } catch (error) {
            Bootstrap.logger.log("error","Error in ProductRepository.deleteProduct " + error);
            throw new Error();
        }
    }
}